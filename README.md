# Estudo de kubernetes

Baseado nas aulas da Alura

## Comandos comuns:

* busca os nós

kubectl get nodes

* cria pod

kubectl run

kubectl run nginx-pod --image=nginx:latest

* busca status dos pods

kubectl get pods

kubectl get pods --watch

kubectl get pods -o wide

* descreve  

kubectl describe nginx-pod

*

kubectl edit pod nginx-pod

*

kubectl apply -f ./primeiro-pod.yaml

*

kubectl delete pod nginx-pod

kubectl delete -f ./primeiro-pod.yaml

---
Os comandos necessários para a instalação e inicialização do cluster no Linux podem ser obtidos abaixo:

Primeiro para o kubectl:

sudo apt-get install curl -y
curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
Agora para o minikube:

curl -Lo minikube https://storage.googleapis.com/minikube/releases/v1.12.1/minikube-linux-amd64 \ && chmod +x minikube
sudo install minikube /usr/local/bin/



--- redhat

cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum install -y kubectl

kubectl run nginx-pod --image=nginx:latest

---

O Kubernetes possui uma vasta integração com diversas plataformas de nuvem, todas essas informações podem ser acessadas através da documentação oficial: https://v1-18.docs.kubernetes.io/docs/concepts/cluster-administration/cloud-providers/